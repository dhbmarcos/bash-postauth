package_version=$(shell bash -c "cat all/DEBIAN/control | grep Version | cut -f2 -d: | xargs")
package_name=$(shell bash -c "cat all/DEBIAN/control | grep Package | cut -f2 -d: | xargs")

package: 
	@chmod 0775 all/DEBIAN/*
	@dpkg-deb --build all
	@mv all.deb $(package_name)_$(package_version)_all.deb
	@echo "'all.deb' moved to '$(package_name)_$(package_version)_all.deb'."

clean: 
	@rm -rf $(package_name)_$(package_version)_all.deb

install: package
	sudo apt install ./$(package_name)_$(package_version)_all.deb

purge: clean
	sudo apt purge $(package_name)
