# Post Authentication Scripts Folders for Bash
Create /etc/bash-postauth/login and /etc/bash-postauth/logout directories
many scripts in login and logout.
The content of files bash.bashrc and bash.bash_logout are moved to
bash-postauth/login/default and bash-postauth/logout/default, repectively.

Copyright 2021 D. H. B. Marcos
