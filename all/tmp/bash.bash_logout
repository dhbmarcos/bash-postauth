#!/bin/bash
#
#   DO NOT CHANGE this SCRIPT!
#
# For change logout and login behaviors, manage script files in /etc/bash-login
# and /etc/bash-logout.

if [ -d /etc/bash-postauth/logout ]
then
    _scripts=$(ls -A /etc/bash-postauth/logout/* | xargs)

    if [ "${_scripts}" ]
    then
        for _script in ${_scripts}
        do
            . "${_script}";
        done;
    fi;
fi;
